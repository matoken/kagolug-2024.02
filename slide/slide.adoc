= Gadgetbridge 経由でスマートウォッチで天気，アクティビティ記録
Kenichiro Matohara(matoken) <matoken@kagolug.org>
:revnumber: 1.0
:revdate: 2024-02-18(sun)
:revremark: 「{doctitle}」
:homepage: https://matoken.org/
:imagesdir: resources
:data-uri:
//:example-caption: 例
//table-caption: 表
//figure-caption: 図
:backend: revealjs
:revealjs_theme: solarized
:customcss: resources/my-css.css
:revealjs_slideNumber: c/t
:title-slide-transition: none
:icons: font
:revealjsdir: reveal.js/
:revealjs_hash: true
:revealjs_center: true
:revealjs_autoPlayMedia: true
:revealjs_transition: false
:revealjs_transitionSpeed: fast
:revealjs_plugin_search: enabled

== link:https://matoken.org[Kenichiro Matohara(matoken) https://matoken.org]

image::map.jpg[background, size=cover, width=80%]
* matoken @matoken@inari.opencocon.org
* 鹿児島の右下の山奥から参加
* 好きなLinuxディストリビューションはDebian

map: © OpenStreetMap contributors

== Gadgetbridge 経由でスマートウォッチで天気，アクティビティ記録

[.columns]
=== 「安めのスマートウォッチをOSSでセキュアに使う(Gadgetbridge)」

[.column.is-one-third]
--
image:slide-kagolug.2023-09.jpg[]
--

[.column]
--
* link:https://kagolug.connpass.com/event/295032/[「鹿児島Linux勉強会 2023.09(オンライン開催)」]
* link:https://gitlab.com/matoken/kagolug-2023-09/-/blob/main/slide/slide.adoc[資料]
--

=== !

image:gadgetbridge-logo.svg[]

* スマートウォッチのスマホアプリはセンシティブな情報を扱うので使いたくない
* OSS のGadgetbridge を使うとスマート ウォッチ、バンド、ヘッドフォンなどのさまざまなガジェットを管理，ペアリング出来る
** 前回試さなかった天気，アクティビティの記録などを試みた

== 天気

* Gadgetbridge だけでは天気情報を取得できない
* Gadgetbridge と連携できるAndroid 天気アプリを導入して連携することで天気情報を表示できるようになる
* 4種類(+LineageOS) のAndroid 天気アプリがGadgetbridge と連携できる
** Tiny Weather Forecast Germany, Weather notification, QuickWeather, Breezy Weather
** 今回は QuickWeather を利用
* link:https://gadgetbridge.org/basics/features/weather/[Weather - Gadgetbridge]
* link:https://gadgetbridge.org/basics/integrations/weather/[Weather providers - Gadgetbridge]

=== !

image:weather-dia.jpg[width=175%]


=== QuickWeather の導入

image:quickweathe.png[width=15%]

* F-Droid からアプリを導入
** link:https://f-droid.org/packages/com.ominous.quickweather/[QuickWeather | F-Droid - Free and Open Source Android App Repository]

[.columns]
=== 天気プロバイダーの選択

[.column]
--
* OpenWeatherMap かOpen-Meteo が選択可能
* OpenWeatherMap はAPI key が必須(無料あり)
* Open-Meteo はAPI key はoption
* 今回はよく使っているOpenWeatherMap を利用した
--

[.column]
--
image:weatherprovider.jpg[]
--

[.columns]
=== Locations 設定

[.column]
--
* 今居る場所はTHIS LOCATION でGPS から取得，その他の場所はOTHER LOCATION から地名や地図から選べる
* 複数の地域を登録可能
--

[.column]
--
image:weatherlocations.jpg[]
--

[.columns]
=== Gadgetbridge 連携

[.column]
* 「Extentions」で「Gadgetbridge」を「ENABLED」に

[.column]
image:weather_extentions.jpg[width=60%]

[.columns]
=== Location 設定2

[.column]
--
* Location を複数設定した場合期待した地域が表示されない場合がある
* QuickWeather アプリの右上のハンバーガーメニューから「Locations」が選択できるのでそこで選択したLocation の天気が時計に転送される
--

[.column]
--
image:weatherlocation-select.jpg[]
--

=== watch

* Weather Provider → QuickWeather → Gadgetbridge → スマートウォッチ

image:watch_weather01.jpg[]
image:watch_weather02.jpg[]

== スマートウォッチからのアクティビティの記録

* スマートウォッチからアクティビティの記録を開始
* スマートフォンのGNSS, スマートウォッチのHR を使いスマートフォン側で記録
* スマートウォッチでアクティビティの記録を停止

* link:https://gadgetbridge.org/basics/integrations/sports/[Sports tracking apps - Gadgetbridge]

=== !

image:activity-dia.jpg[]

[.columns]
=== Gadgetbridge 設定

[.column]
--
* Gadgetbridge の設定でHR を他アプリから利用できるようにしておく
** 該当デバイスの「Device soecitic settings」から「サードパーティのリアルタイムHRアクセス」と「Visible while connected」をon にする
--

[.column]
--
image:gb-allow-remote.jpg[]
--

=== Android にトラッキングアプリを導入

image:opentracks.png[width=10%]
image:fitotrack.png[width=10%]

* OpenTracks, FitoTrack などが利用可能
* 今回は以前利用したことのある OpenTracks をF-Droid から導入して利用
** link:https://f-droid.org/packages/de.dennisguse.opentracks/[OpenTracks | F-Droid - Free and Open Source Android App Repository]

[.columns]
=== OpenTracks 設定

[.column]
--
* Gadgetbridge からHR を利用するための設定を行う
** 右上のハンバーガーメニューから「設定」
** Bluetooth Sensors
** 心拍数 -> スマートウォッチを選択
--

[.column]
image:opentracks-hr.jpg[width=50%]

[.columns]
=== アクティビティの開始

[.column]
* スマートウォッチからアクティビティの記録を開始するとスマートフォンで記録が開始される

image:opentracks-record.jpg[]

[.columns]
=== crashed💣

[.column]
--
* OpenTracks を使っていないときにたまにcrashe(週に数回程?)
* 02/13 リリースの v4.11.1 で治った?
----
v4.11.1: OpenTracks

Bugfix:
- PublicAPI crashed when stopping
----
--

[.column]
--
image:opentracks-crashed.jpg[]
--

=== log 活用例

* link:https://activityworkshop.net/software/gpsprune/index.html[GpsPrune], link:https://www.gpsbabel.org/[GPSBabel], link:https://josm.openstreetmap.de/[JOSM] ……
* Rlive にUp して動画に https://www.relive.cc/
** GPS log からGoogleMap を使い動画を作成，[.line-through]#HR 情報があると最大心拍数なども動画に表示される# link:https://support.relive.cc/kb/guide/en/heart-rate-and-power-data-N43GJ06qrr/Steps/34458[非対応になってる?]

== 残りの課題

* Firmware Update
* 時計フェイス着替え

FW 入手がとても面倒&失敗すると壊れる可能性が?

== まとめ

* 面倒で設定していなかったスマートウォッチでの天気表示とアクティビティの記録操作(とHR記録)
* 設定してしまえば簡単だった
* しかし手持ちの時計は小さく操作しづらいのもあってスマートフォンを使いがち
** 例えば天気を表示するのに8ステップ必要
** もう少し大きな時計だと便利かも?
* 雨の日などにかばんの中にスマホをしまっているときなどに使える?

== 奥付

発表::
* link:https://kagolug.connpass.com/event/309453/[鹿児島Linux勉強会 2024.02(オンライン開催)] 2024-02-18(sun)
発表者::
* link:https://matoken.org/[Kenichiro Matohara(matoken)]
利用ソフトウェア::
* link:https://github.com/asciidoctor/asciidoctor-reveal.js[Asciidoctor Reveal.js]
ライセンス::
* link:https://creativecommons.org/licenses/by/4.0/[CC BY 4.0]

